package com.example.domain;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthenticationInterceptor implements Interceptor {

    @NonNull
    private final String mAuthToken;

    public AuthenticationInterceptor(@NonNull String authToken) {
        mAuthToken = authToken;
    }

    @Override
    @NonNull
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request newRequest = request.newBuilder().header("Authorization", mAuthToken)
                                    .build();
        return chain.proceed(newRequest);
    }
}
