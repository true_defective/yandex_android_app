package com.example.domain;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

public interface NoteInteractor {
    void fetchNotes(@NonNull MutableLiveData<Boolean> isFetching);
    void dispose();
}
