package com.example.domain;

public interface OnBackPressedListener {
    /**
     * @return true if parent has to handle back press
     */
    boolean onBackPressed();
}
