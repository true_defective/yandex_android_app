package com.example.domain;

import android.support.annotation.NonNull;

import java.util.Comparator;

public class NoteComparatorHelper {
    public static final String DATE_DESCDENDING = "date_descending";
    public static final String DATE_ASCDENDING = "date_ascending";
    public static final String DEFAULT_SORT = DATE_DESCDENDING;

    private static final Comparator<Note> DATE_DESCDENDING_COMPARATOR = (o1, o2) -> o2.date.compareTo(o1.date);
    private static final Comparator<Note> DATE_ASCDENDING_COMPARATOR = (o1, o2) -> o1.date.compareTo(o2.date);

    public static final Comparator<Note> DEFAULT_COMPARATOR = getComparator(DEFAULT_SORT);

    @NonNull
    public static Comparator<Note> getComparator(@NoteComparatorDef @NonNull String comparatorName) {
        if (comparatorName.equals(DATE_DESCDENDING)) {
            return DATE_DESCDENDING_COMPARATOR;
        }

        return DATE_ASCDENDING_COMPARATOR;
    }
}
