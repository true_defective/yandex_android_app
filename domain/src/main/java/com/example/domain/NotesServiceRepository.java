package com.example.domain;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Callback;

public interface NotesServiceRepository {
    @NonNull
    Single<List<Note>> getNotes();
    @NonNull
    Single<Note> insert(@NonNull Note note);
    void update(@NonNull Note note);
    void delete(@NonNull Note note);
}
