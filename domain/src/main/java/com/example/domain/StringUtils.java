package com.example.domain;

import android.os.Bundle;
import android.support.annotation.NonNull;

public class StringUtils {

    @NonNull
    public static String bundleRequireString(@NonNull Bundle bundle, @NonNull String key) {
        final String value = bundle.getString(key);
        if (value == null) {
            throw new IllegalStateException("Bundle required string cant be null");
        }

        return value;
    }
}
