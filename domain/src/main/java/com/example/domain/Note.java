package com.example.domain;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Note {
    private static final int AUTO_ID = 0;

    public int id;
    @NonNull
    @SerializedName("title")
    public String header;
    @NonNull
    @SerializedName("description")
    public String body;
    @NonNull
    @SerializedName("creation_data")
    public Date date;
    @NonNull
    @SerializedName("color")
    public int color;

    @Expose
    public int outsideId;

    public Note(@NonNull String header, @NonNull String body, int color) {
        this(AUTO_ID, header, body, new Date(), color);
    }

    public Note(int id, @NonNull String header, @NonNull String body, @NonNull Date date, int color) {
        this.id = id;
        this.header = header;
        this.body = body;
        this.date = date;
        this.color = color;
    }

    public Note(int id, @NonNull String header, @NonNull String body, @NonNull Date date, int color, int outsideId) {
        this.id = id;
        this.header = header;
        this.body = body;
        this.date = date;
        this.color = color;
        this.outsideId = outsideId;
    }
    
    public void setAutoId() {
        outsideId = id;
        id = AUTO_ID;
    }

    public boolean hasEqualContent(@NonNull Note otherNote) {
        return header.equals(otherNote.header) && body.equals(otherNote.body);
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof Note) {
            final Note otherNote = (Note) obj;
            return id == otherNote.id && header.equals(otherNote.header) && body.equals(otherNote.body)
                    && date.equals(otherNote.date) && color == otherNote.color;
        }

        return false;
    }
}
