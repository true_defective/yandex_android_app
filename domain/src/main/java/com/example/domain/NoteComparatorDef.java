package com.example.domain;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.example.domain.NoteComparatorHelper.DATE_ASCDENDING;
import static com.example.domain.NoteComparatorHelper.DATE_DESCDENDING;

@Retention(RetentionPolicy.SOURCE)
@StringDef({DATE_DESCDENDING, DATE_ASCDENDING})
public @interface NoteComparatorDef {}
