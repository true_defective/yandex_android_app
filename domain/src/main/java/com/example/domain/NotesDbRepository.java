package com.example.domain;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import io.reactivex.Flowable;

public interface NotesDbRepository {
    void fetchNotes(@NonNull MutableLiveData<Boolean> isFetchingData, @Nullable List<Note> notes);
    void update(@NonNull Note note);
    void insert(@NonNull Note note);
    void delete(@NonNull Note note);
    Flowable<List<Note>> getAllNotes();
}
