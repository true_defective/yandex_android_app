package com.example.askeip.notebook;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.domain.KeyboardHelper;
import com.example.domain.Note;
import com.example.domain.NoteComparatorDef;
import com.example.domain.NoteComparatorHelper;
import com.example.domain.OnBackPressedListener;

import javax.inject.Inject;

public class NotesFragment extends Fragment implements OnBackPressedListener {
    private static final String BOTTOM_SHEET_STATE_KEY = "bottom_sheet_key";

    @Nullable
    private String mFilter;
    @NonNull
    private String mComparatorName = NoteComparatorHelper.DATE_DESCDENDING;
    private FilterNotesFragment mFilterNotesFragment;

    private BottomSheetBehavior mBottomSheetBehavior;
    @NonNull
    private final ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(
            new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
                @Override
                public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                    int i = viewHolder.getAdapterPosition();
                    final Note note = mNotesAdapter.get(i);
                    mModel.remove(note);
                }
            });
    @NonNull
    private final BottomSheetBehavior.BottomSheetCallback mBottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View view, int i) {
            if (i == BottomSheetBehavior.STATE_COLLAPSED) {
                KeyboardHelper.hideKeyboard(requireActivity());
            }
        }

        @Override
        public void onSlide(@NonNull View view, float v) {
        }
    };

    private ProgressBar mProgressBar;

    private NotesAdapter mNotesAdapter;
    private boolean mHasToCleanSearch;
    private NotesViewModel mModel;

    @Inject
    NotesViewModelFactory mFactory;

    @NonNull
    public static NotesFragment newInstance() {
        return new NotesFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((NoteApplication)requireActivity().getApplication()).getComponent().inject(this);
        mModel = ViewModelProviders.of(requireActivity(), mFactory).get(NotesViewModel.class);
        if (savedInstanceState != null) {
            mModel.applyBundleIfNeeded(savedInstanceState);
        }
        mNotesAdapter = new NotesAdapter(note -> editNote(note.id));
        mModel.getFilteredNotes().observe(this, list -> mNotesAdapter.submitList(list));
        mModel.getComparatorName().observe(this, this::setComparator);
        mModel.getFilter().observe(this, s -> mFilter = s);
        final String comparatorName = mModel.getComparatorName().getValue();
        mComparatorName = comparatorName == null ? NoteComparatorHelper.DEFAULT_SORT : comparatorName;
        mFilter = mModel.getFilter().getValue();

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_notes, container, false);
        RecyclerView mRecyclerView = rootView.findViewById(R.id.notes_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        if (container == null) {
            throw new IllegalStateException("Container required for NotesFragment");
        }
        mProgressBar = requireActivity().findViewById(R.id.main_progress_bar);
        mModel.getFetchingData().observe(this, flag -> {
            if (mProgressBar == null || flag == null) {
                return;
            }
            mProgressBar.setVisibility(flag ? View.VISIBLE : View.GONE);
        });

        mBottomSheetBehavior = BottomSheetBehavior.from(rootView.findViewById(R.id.notes_bottom_sheet_container));
        mBottomSheetBehavior.setBottomSheetCallback(mBottomSheetCallback);

        mRecyclerView.setAdapter(mNotesAdapter);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
        View addNoteButton = rootView.findViewById(R.id.notes_add_note_button);
        addNoteButton.setOnClickListener(v -> addNote());
        if (savedInstanceState == null) {
            if (getChildFragmentManager().getFragments().size() == 0) {
                FilterNotesFragment filterNotesFragment = FilterNotesFragment.newInstance();
                getChildFragmentManager().beginTransaction().add(R.id.notes_bottom_sheet_container, filterNotesFragment, FilterNotesFragment.TAG).commit();
            }
            mHasToCleanSearch = true;
        } else {
            mBottomSheetBehavior.setState(savedInstanceState.getInt(BOTTOM_SHEET_STATE_KEY));
            mHasToCleanSearch = false;
        }

        return rootView;
    }

    @Override
    public void onStart() {
        Fragment fragment = getChildFragmentManager().findFragmentByTag(FilterNotesFragment.TAG);
        if (fragment instanceof FilterNotesFragment) {
            mFilterNotesFragment = (FilterNotesFragment) fragment;
        } else {
            throw new IllegalStateException("NotesFragment should have FilterNotesFragment child");
        }
        super.onStart();
    }

    @Override
    public void onResume() {
        if (mHasToCleanSearch) {
            mFilterNotesFragment.cleanSearch();
        }
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.notes_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add_note) {
            addNote();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void addNote() {
        Fragment fragment = EditNoteFragment.newInstance();
        startEditNoteFragment(fragment);
    }

    private void editNote(int id) {
        Fragment fragment = EditNoteFragment.newInstance(id);
        startEditNoteFragment(fragment);
    }

    private void startEditNoteFragment(@NonNull Fragment fragment) {
        mProgressBar.setVisibility(View.GONE);
        requireActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment_container, fragment)
                .addToBackStack(null)
                .commit();
        resetFilterNotesFragment();
    }

    private void resetFilterNotesFragment() {
        mFilterNotesFragment.cleanSearch();
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString(NotesViewModelImpl.COMPARATOR_KEY, mComparatorName);
        outState.putString(NotesViewModelImpl.FILTER_KEY, mFilter);
        outState.putInt(BOTTOM_SHEET_STATE_KEY, mBottomSheetBehavior.getState());
        super.onSaveInstanceState(outState);
    }

    private void setComparator(@NoteComparatorDef @NonNull String comparatorName) {
        mComparatorName = comparatorName;
    }

    @Override
    public boolean onBackPressed() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
        if (mBottomSheetBehavior.getState() != BottomSheetBehavior.STATE_COLLAPSED) {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            return false;
        }
        return true;
    }
}
