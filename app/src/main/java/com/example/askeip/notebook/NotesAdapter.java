package com.example.askeip.notebook;

import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.domain.Note;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class NotesAdapter extends ListAdapter<Note, NotesAdapter.ViewHolder> {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm dd-MM-yyyy", Locale.US);
    @NonNull
    private OnItemClickListener mOnItemClickListener;

    NotesAdapter(@NonNull OnItemClickListener onItemClickListener) {
        super(new DiffUtil.ItemCallback<Note>() {
            @Override
            public boolean areItemsTheSame(@NonNull Note note, @NonNull Note t1) {
                return note.hashCode() == t1.hashCode();
            }

            @Override
            public boolean areContentsTheSame(@NonNull Note note, @NonNull Note t1) {
                return note.equals(t1);
            }
        });
        mOnItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_note, parent, false);
        return new ViewHolder(view);
    }

    @NonNull
    Note get(int i) {
        return getItem(i);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(getItem(i));
    }

    public interface OnItemClickListener {
        void OnItemClick(@NonNull Note note);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private static final int borderWidth = 10;

        @NonNull
        private View parent;
        @NonNull
        private GradientDrawable background;
        @NonNull
        private TextView headerTextView;
        @NonNull
        private TextView bodyTextView;
        @NonNull
        private TextView dateTextView;

        ViewHolder(@NonNull View view) {
            super(view);
            parent = view;
            background = (GradientDrawable) view.getBackground();
            headerTextView = view.findViewById(R.id.note_header);
            bodyTextView = view.findViewById(R.id.note_body);
            dateTextView = view.findViewById(R.id.note_date);
        }

        void bind(@NonNull final Note note) {
            background.setStroke(borderWidth, note.color);
            headerTextView.setText(note.header);
            bodyTextView.setText(note.body);
            dateTextView.setText(DATE_FORMAT.format(note.date));
            parent.setOnClickListener(v -> mOnItemClickListener.OnItemClick(note));
        }
    }
}
