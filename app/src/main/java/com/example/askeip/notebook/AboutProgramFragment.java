package com.example.askeip.notebook;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class AboutProgramFragment extends Fragment {
    private static final String TITLE = "About";

    public static AboutProgramFragment newInstance() {
        Bundle args = new Bundle();

        AboutProgramFragment fragment = new AboutProgramFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about_program, container, false);

        return rootView;
    }

}