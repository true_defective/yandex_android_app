package com.example.askeip.notebook;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.domain.NoteComparatorHelper;

import javax.inject.Inject;

public class FilterNotesFragment extends Fragment {
    public static final String TAG = "FilterNotesFragment";

    @NonNull
    private final TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mModel.setFilter(s.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private EditText mSearchEditText;
    private NotesViewModel mModel;

    @Inject
    NotesViewModelFactory mFactory;

    @NonNull
    public static FilterNotesFragment newInstance() {
        return new FilterNotesFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((NoteApplication)requireActivity().getApplication()).getComponent().inject(this);
        mModel = ViewModelProviders.of(requireActivity(), mFactory).get(NotesViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_filter_notes, container, false);
        View sortDateDescendingView = rootView.findViewById(R.id.filter_notes_date_descending);
        sortDateDescendingView.setOnClickListener(v -> mModel.setComparatorName(NoteComparatorHelper.DATE_DESCDENDING));
        View sortDateAscendingView = rootView.findViewById(R.id.filter_notes_date_ascending);
        sortDateAscendingView.setOnClickListener(v -> mModel.setComparatorName(NoteComparatorHelper.DATE_ASCDENDING));
        mSearchEditText = rootView.findViewById(R.id.filter_notes_search);
        mSearchEditText.addTextChangedListener(mTextWatcher);

        return rootView;
    }

    public void cleanSearch() {
        mSearchEditText.setText(null);
    }
}
