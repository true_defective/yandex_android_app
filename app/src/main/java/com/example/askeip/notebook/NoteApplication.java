package com.example.askeip.notebook;

import android.app.Application;
import android.support.annotation.NonNull;

import com.example.askeip.notebook.di.ApplicationComponent;
import com.example.askeip.notebook.di.ApplicationModule;
import com.example.askeip.notebook.di.DaggerApplicationComponent;
import com.example.askeip.notebook.di.NotesServiceRepositoryModule;

import okhttp3.Credentials;

public class NoteApplication extends Application {

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationComponent = DaggerApplicationComponent.builder()
                                                          .applicationModule(new ApplicationModule(this))
                                                          .notesServiceRepositoryModule(new NotesServiceRepositoryModule(
                                                                  Credentials.basic("notes_user",
                                                                                    "notes_password")))
                                                          .build();
    }

    @NonNull
    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }
}
