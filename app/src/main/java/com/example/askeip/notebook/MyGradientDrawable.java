package com.example.askeip.notebook;

import android.graphics.drawable.GradientDrawable;

public class MyGradientDrawable extends GradientDrawable {
    private int savedColor;

    public int GetSavedColor() {
        return savedColor;
    }

    @Override
    public void setColor(int argb) {
        savedColor = argb;
        super.setColor(argb);
    }
}
