package com.example.askeip.notebook.di;

import android.support.annotation.NonNull;

import com.example.askeip.notebook.NotesViewModelFactory;
import com.example.domain.NoteInteractor;
import com.example.domain.NotesDbRepository;
import com.example.domain.NotesServiceRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
class ViewModelModule {

    @Singleton
    @Provides
    NotesViewModelFactory provideNotesViewModelFactory(@NonNull NotesServiceRepository notesServiceRepository,
                                                       @NonNull NotesDbRepository notesDbRepository,
                                                       @NonNull NoteInteractor noteInteractor) {
        return new NotesViewModelFactory(notesServiceRepository, notesDbRepository, noteInteractor);
    }
}
