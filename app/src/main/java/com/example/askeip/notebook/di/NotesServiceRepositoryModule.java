package com.example.askeip.notebook.di;

import android.support.annotation.NonNull;

import com.example.data.NoteServiceApi;
import com.example.data.NotesServiceRepositoryImpl;
import com.example.domain.AuthenticationInterceptor;
import com.example.domain.NotesServiceRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NotesServiceRepositoryModule {
    private static final String API_BASE_URL = "http://10.0.2.2:8000/";

    @NonNull
    private final String mAuthToken;

    public NotesServiceRepositoryModule(@NonNull String authToken) {
        mAuthToken = authToken;
    }

    @Singleton
    @Provides
    NotesServiceRepository provideNotesServiceRepository(@NonNull NoteServiceApi noteServiceApi) {
        return new NotesServiceRepositoryImpl(noteServiceApi);
    }

    @Provides
    NoteServiceApi provideNoteServiceApi(@NonNull Retrofit retrofit) {
        return retrofit.create(NoteServiceApi.class);
    }

    @Provides
    Retrofit provideRetrofit(@NonNull OkHttpClient okHttpClient, @NonNull GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .client(okHttpClient)
                .build();
    }

    @Provides
    GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    OkHttpClient provideOkHttpClient(@NonNull Interceptor interceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
    }

    @Provides
    Interceptor provideInterceptor() {
        return new AuthenticationInterceptor(mAuthToken);
    }
}
