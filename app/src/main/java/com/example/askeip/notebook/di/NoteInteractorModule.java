package com.example.askeip.notebook.di;

import android.support.annotation.NonNull;

import com.example.data.NoteInteractorImpl;
import com.example.domain.NoteInteractor;
import com.example.domain.NotesDbRepository;
import com.example.domain.NotesServiceRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class NoteInteractorModule {
    @Singleton
    @Provides
    public NoteInteractor provideNoteInteractor(@NonNull NotesServiceRepository notesServiceRepository,
                                                @NonNull NotesDbRepository notesDbRepository) {
        return new NoteInteractorImpl(notesServiceRepository, notesDbRepository);
    }
}
