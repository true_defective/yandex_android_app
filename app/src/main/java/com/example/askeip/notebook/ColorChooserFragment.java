package com.example.askeip.notebook;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

public class ColorChooserFragment extends Fragment {
    private static final String SAVED_STATE_COLOR_KEY = "saved_color_key";

    public static final String COLOR_ARGS = "color_args";
    private static final int squareMarginHorizontalDivider= 4;
    private static final int squareMarginVerticalDivider= 8;
    private static final int borderWidth = 3;

    private View viewChosenColor;
    private TextView tvRGBChosenColor;
    private TextView tvHSVChosenColor;
    private LinearLayout colorsHolder;

    private static ArrayList<String> colors =
            new ArrayList<>(Arrays.asList("#f44242", "#f47a41", "#f4cd41", "#c1f441",
                                          "#7ff441", "#41f461", "#41f4a0", "#41f4dc",
                                          "#41d0f4", "#4191f4", "#4158f4", "#7f41f4",
                                          "#c141f4", "#eb41f4", "#f441af", "#f44161"));
    private int[] intColors;

    private int mColor;

    public static ColorChooserFragment newInstance(int color) {
        
        Bundle args = new Bundle();
        args.putInt(COLOR_ARGS, color);

        ColorChooserFragment fragment = new ColorChooserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle args = getArguments();
        if (args != null) {
            mColor = args.getInt(COLOR_ARGS, getResources().getColor(R.color.defaultNoteColor));
        }
        if (savedInstanceState != null) {
            mColor = savedInstanceState.getInt(SAVED_STATE_COLOR_KEY);
        }
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_color_chooser, container, false);
        setHasOptionsMenu(true);

        viewChosenColor = rootView.findViewById(R.id.chosen_color);
        tvRGBChosenColor = rootView.findViewById(R.id.rgb_chosen_color);
        tvHSVChosenColor = rootView.findViewById(R.id.hsv_chosen_color);
        colorsHolder = rootView.findViewById(R.id.colors_holder);

        intColors = new int[colors.size()];
        for (int i = 0; i < colors.size(); i++) {
            intColors[i] = Color.parseColor(colors.get(i));
        }

        createViews();
        setColorValue(mColor);

        return rootView;
    }

    private void setColorValue(int color) {
        mColor = color;
        viewChosenColor.setBackgroundColor(mColor);
        float[] hsv = new float[3];
        Color.colorToHSV(mColor, hsv);
        tvRGBChosenColor.setText(String.format("#%06X", (0xFFFFFF & mColor)));
        tvHSVChosenColor.setText(String.format("H:%s S:%s V:%s", (int)hsv[0], (int)hsv[1], (int)hsv[2]));
    }

    private void createViews() {

        int squareSide = (int)getResources().getDimension(R.dimen.color_picker_square_size);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(squareSide, squareSide);
        layoutParams.setMargins(squareSide/squareMarginHorizontalDivider,
                                squareSide/squareMarginVerticalDivider,
                                squareSide/squareMarginHorizontalDivider,
                                squareSide/squareMarginVerticalDivider);

        for (int i = 0; i < intColors.length; i++) {
            View myView = new View(getActivity());
            myView.setLayoutParams(layoutParams);
            MyGradientDrawable background = new MyGradientDrawable();
            background.setColor(intColors[i]);
            background.setStroke(borderWidth, Color.BLACK);
            myView.setBackground(background);

            myView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyGradientDrawable mgd = (MyGradientDrawable) view.getBackground();
                    setColorValue(mgd.GetSavedColor());
                }
            });
            colorsHolder.addView(myView);
        }
        drawGradient();
    }

    private void drawGradient() {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);

        int[] gradientColors = new int[intColors.length*2 + 1];
        gradientColors[0] = intColors[0];
        gradientColors[1] = intColors[0];
        for (int i = 1; i < intColors.length; i++) {
            float[] hsvLeft = new float[3];
            Color.colorToHSV(intColors[i-1], hsvLeft);
            float[] hsvRight = new float[3];
            Color.colorToHSV(intColors[i], hsvRight);
            float[] hsvMiddle = new float[] {(hsvLeft[0] + hsvRight[0])/2, (hsvLeft[1] + hsvRight[1])/2, (hsvLeft[2] + hsvRight[2])/2};
            gradientColors[i*2] = Color.HSVToColor(hsvMiddle);
            gradientColors[i*2+1] = intColors[i];
        }
        gradientColors[gradientColors.length - 1] = intColors[intColors.length - 1];

        gradientDrawable.setColors(gradientColors);
        colorsHolder.setBackground(gradientDrawable);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.color_chooser_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            saveChoice();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveChoice() {
        Fragment fragment = getTargetFragment();
        FragmentManager fragmentManager = getFragmentManager();
        if (fragment == null || fragmentManager == null) {
            return;
        }
        Intent data = new Intent();
        data.putExtra(EditNoteFragment.INTENT_COLOR_KEY, mColor);
        fragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
        fragmentManager.popBackStack();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVED_STATE_COLOR_KEY, mColor);
    }
}
