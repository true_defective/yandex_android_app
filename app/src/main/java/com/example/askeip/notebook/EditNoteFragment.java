package com.example.askeip.notebook;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.domain.Note;
import com.example.domain.OnBackPressedListener;

import javax.inject.Inject;

public class EditNoteFragment extends Fragment implements OnBackPressedListener {
    private static final int NO_ID = -1;
    private static final String SAVED_STATE_COLOR_KEY = "saved_color_key";

    public static final int COLOR_PICKER_REQUEST_CODE = 213;
    public static final String INTENT_COLOR_KEY = "intent_color_key";
    public static final String ID_KEY = "id_key";

    private EditText mHeaderEditText;
    private EditText mBodyEditText;
    private int mNoteColor;

    private int mNoteId = NO_ID;
    private NotesViewModel mModel;

    @Inject
    NotesViewModelFactory mFactory;

    @NonNull
    public static EditNoteFragment newInstance() {
        return newInstance(NO_ID);
    }

    @NonNull
    public static EditNoteFragment newInstance(int id) {

        Bundle args = new Bundle();
        args.putInt(ID_KEY, id);
        EditNoteFragment fragment = new EditNoteFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((NoteApplication)requireActivity().getApplication()).getComponent().inject(this);
        mModel = ViewModelProviders.of(requireActivity(), mFactory).get(NotesViewModel.class);
        final Bundle args = getArguments();
        if (args != null) {
            mNoteId = args.getInt(ID_KEY, NO_ID);
        }
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_edit_note, container, false);
        mHeaderEditText = rootView.findViewById(R.id.edit_note_header);
        mBodyEditText = rootView.findViewById(R.id.edit_note_body);
        if (mNoteId != NO_ID) {
            final Note note = mModel.getNote(mNoteId);
            mHeaderEditText.setText(note.header);
            mBodyEditText.setText(note.body);
            if (mNoteColor == 0) {
                mNoteColor = note.color;
            }
        }
        if (mNoteColor == 0) {
            if (savedInstanceState != null) {
                mNoteColor = savedInstanceState.getInt(SAVED_STATE_COLOR_KEY);
            } else {
                mNoteColor = getResources().getColor(R.color.defaultNoteColor);
            }
        }

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.edit_note_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                saveNote();
                return true;
            case R.id.action_edit_color:
                editColor();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void editColor() {
        Fragment fragment = ColorChooserFragment.newInstance(mNoteColor);
        fragment.setTargetFragment(this, COLOR_PICKER_REQUEST_CODE);
        requireActivity().getSupportFragmentManager()
                         .beginTransaction()
                         .replace(R.id.main_fragment_container, fragment)
                         .addToBackStack(null)
                         .commit();
    }

    @Override
    public boolean onBackPressed() {
        saveNote();
        return false;
    }

    public void saveNote() {
        String header = mHeaderEditText.getText().toString();
        String body = mBodyEditText.getText().toString();
        if (header.isEmpty()) {
            if (body.isEmpty()) {
                finish();
                return;
            }
            header = body;
        }
        if (mNoteId == NO_ID) {
            mModel.addNote(new Note(header, body, mNoteColor));
        } else {
            final Note note = mModel.getNote(mNoteId);
            mModel.editNote(new Note(note.id, header, body, note.date, mNoteColor, note.outsideId));
        }
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == COLOR_PICKER_REQUEST_CODE) {
            if (data != null && data.hasExtra(INTENT_COLOR_KEY)) {
                mNoteColor = data.getIntExtra(INTENT_COLOR_KEY, mNoteColor);
            }
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVED_STATE_COLOR_KEY, mNoteColor);
    }

    private void finish() {
        requireActivity().getSupportFragmentManager()
                .popBackStackImmediate();
    }
}
