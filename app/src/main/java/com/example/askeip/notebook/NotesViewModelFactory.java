package com.example.askeip.notebook;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.example.domain.NoteInteractor;
import com.example.domain.NotesDbRepository;
import com.example.domain.NotesServiceRepository;

public class NotesViewModelFactory implements ViewModelProvider.Factory {

    @NonNull
    private final NotesServiceRepository mNotesServiceRepository;
    @NonNull
    private final NotesDbRepository mNotesDbRepository;
    @NonNull
    private final NoteInteractor mNoteInteractor;

    public NotesViewModelFactory(@NonNull NotesServiceRepository notesServiceRepository,
                                 @NonNull NotesDbRepository notesDbRepository,
                                 @NonNull NoteInteractor noteInteractor) {
        mNotesServiceRepository = notesServiceRepository;
        mNotesDbRepository = notesDbRepository;
        mNoteInteractor = noteInteractor;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(NotesViewModelImpl.class)) {
            return (T) new NotesViewModelImpl(mNotesServiceRepository, mNotesDbRepository, mNoteInteractor);
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
