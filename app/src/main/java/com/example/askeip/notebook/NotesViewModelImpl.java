package com.example.askeip.notebook;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.LiveDataReactiveStreams;
import android.arch.lifecycle.MutableLiveData;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.domain.Note;
import com.example.domain.NoteComparatorDef;
import com.example.domain.NoteComparatorHelper;
import com.example.domain.NoteInteractor;
import com.example.domain.NotesDbRepository;
import com.example.domain.NotesServiceRepository;
import com.example.domain.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

class NotesViewModelImpl extends NotesViewModel {
    static final String COMPARATOR_KEY = "comparator_key";
    static final String FILTER_KEY = "filter_key";

    @NonNull
    private final NotesServiceRepository mNotesServiceRepository;
    @NonNull
    private final NotesDbRepository mNotesDbRepository;
    @NonNull
    private final LiveData<List<Note>> mAllNotes;
    @NonNull
    private final MutableLiveData<List<Note>> mFilteredNotes;
    @NonNull
    private final MutableLiveData<String> mComparatorName;
    @NonNull
    private final MutableLiveData<String> mFilter;
    @NonNull
    private final MutableLiveData<Boolean> mIsFetchingData;
    @NonNull
    private final NoteInteractor mNoteInteractor;
    @NonNull
    private Comparator<Note> mComparator = NoteComparatorHelper.DEFAULT_COMPARATOR;
    @Nullable
    private Disposable mDisposable;

    NotesViewModelImpl(@NonNull NotesServiceRepository notesServiceRepository,
                       @NonNull NotesDbRepository notesDbRepository,
                       @NonNull NoteInteractor noteInteractor) {
        mNotesServiceRepository = notesServiceRepository;
        mNotesDbRepository = notesDbRepository;
        mAllNotes = LiveDataReactiveStreams.fromPublisher(mNotesDbRepository.getAllNotes());
        mComparatorName = new MutableLiveData<>();
        mFilteredNotes = new MutableLiveData<>();
        mFilter = new MutableLiveData<>();
        mIsFetchingData = new MutableLiveData<>();
        mIsFetchingData.setValue(false);
        mAllNotes.observeForever(notes -> updateFilteredNotes());
        mNoteInteractor = noteInteractor;
        fetchServiceNotes();
        setComparatorName(NoteComparatorHelper.DEFAULT_SORT);
    }

    void applyBundleIfNeeded(@NonNull Bundle bundle) {
        if (mFilteredNotes.getValue() == null) {
            setComparatorName(StringUtils.bundleRequireString(bundle, COMPARATOR_KEY));
            setFilter(StringUtils.bundleRequireString(bundle, FILTER_KEY));
        }
    }

    @NonNull
    Note getNote(int id) {
        for (Note note : Objects.requireNonNull(mAllNotes.getValue())) {
            if (note.id == id) {
                return note;
            }
        }

        throw new IllegalArgumentException("Note with given id should exist in AllNotes");
    }

    void setComparatorName(@NonNull @NoteComparatorDef String comparatorName) {
        if (!comparatorName.equals(mComparatorName.getValue())) {
            mComparatorName.setValue(comparatorName);
            mComparator = NoteComparatorHelper.getComparator(mComparatorName.getValue());
            updateFilteredNotes();
        }
    }

    @NonNull
    LiveData<String> getComparatorName() {
        return mComparatorName;
    }

    void setFilter(@Nullable String filter) {
        if (filter == null ? mFilter.getValue() == null : filter.equals(mFilter.getValue())) {
            return;
        }
        mFilter.setValue(filter);
        updateFilteredNotes();
    }

    private void updateFilteredNotes() {
        final String filter = mFilter.getValue();
        List<Note> notes;
        if (filter == null || filter.isEmpty()) {
            notes = mAllNotes.getValue();
            notes = notes != null ? new ArrayList<>(notes) : new ArrayList<>();
        } else {
            final String lowerCaseQuery = filter.toLowerCase();

            final ArrayList<Note> filteredNotes = new ArrayList<>();
            final List<Note> allNotes = mAllNotes.getValue();
            if (allNotes == null) {
                mFilteredNotes.setValue(null);
                return;
            }
            for (Note note: allNotes) {
                final String text = note.header.toLowerCase();
                if (text.contains(lowerCaseQuery)) {
                    filteredNotes.add(note);
                }
            }
            notes = filteredNotes;
        }
        Collections.sort(notes, mComparator);
        mFilteredNotes.setValue(notes);
    }

    @NonNull
    LiveData<String> getFilter() {
        return mFilter;
    }

    @NonNull
    LiveData<List<Note>> getFilteredNotes() {
        return mFilteredNotes;
    }

    @NonNull
    @Override
    LiveData<Boolean> getFetchingData() {
        return mIsFetchingData;
    }

    private void fetchServiceNotes() {
        mIsFetchingData.setValue(true);
        mNoteInteractor.fetchNotes(mIsFetchingData);
    }

    @Override
    protected void onCleared() {
        mNoteInteractor.dispose();
        if (mDisposable != null) {
            mDisposable.dispose();
        }
        super.onCleared();
    }

    void addNote(@NonNull Note note) {
        mNotesDbRepository.insert(note);
        mNotesServiceRepository.insert(note).observeOn(Schedulers.newThread())
                               .subscribe(new SingleObserver<Note>() {
                                   @Override
                                   public void onSubscribe(Disposable d) {
                                       mDisposable = d;
                                   }

                                   @Override
                                   public void onSuccess(Note note) {
                                       mNotesDbRepository.update(note);
                                   }

                                   @Override
                                   public void onError(Throwable e) {
                                   }
                               });
    }

    void editNote(@NonNull Note note) {
        mNotesServiceRepository.update(note);
        mNotesDbRepository.update(note);
        updateFilteredNotes();
    }

    void remove(@NonNull Note note) {
        mNotesServiceRepository.delete(note);
        mNotesDbRepository.delete(note);
        updateFilteredNotes();
    }
}
