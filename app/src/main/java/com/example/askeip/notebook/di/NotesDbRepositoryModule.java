package com.example.askeip.notebook.di;

import android.content.Context;
import android.support.annotation.NonNull;

import com.example.data.NoteDao;
import com.example.data.NoteModelMapperImpl;
import com.example.data.NoteRoomDatabase;
import com.example.data.NotesDbRepositoryImpl;
import com.example.domain.NotesDbRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ApplicationModule.class})
class NotesDbRepositoryModule {

    @Singleton
    @Provides
    NotesDbRepository provideNotesDbRepository(@NonNull NoteDao noteDao, @NonNull NoteModelMapperImpl mapper) {
        return new NotesDbRepositoryImpl(noteDao, mapper);
    }

    @Provides
    NoteModelMapperImpl provideNoteModelMapper() {
        return new NoteModelMapperImpl();
    }

    @Provides
    NoteRoomDatabase provideNoteRoomDatabase(@NonNull Context applicationContext) {
        return NoteRoomDatabase.getDatabase(applicationContext);
    }

    @Provides
    NoteDao provideNoteDao(@NonNull NoteRoomDatabase database) {
        return database.noteDao();
    }
}
