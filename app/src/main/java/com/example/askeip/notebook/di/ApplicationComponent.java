package com.example.askeip.notebook.di;

import com.example.askeip.notebook.EditNoteFragment;
import com.example.askeip.notebook.FilterNotesFragment;
import com.example.askeip.notebook.NotesFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NotesDbRepositoryModule.class, ViewModelModule.class,
        NotesServiceRepositoryModule.class, NoteInteractorModule.class})
public interface ApplicationComponent {

    void inject(NotesFragment notesFragment);
    void inject(FilterNotesFragment filterNotesFragment);
    void inject(EditNoteFragment editNoteFragment);
}
