package com.example.askeip.notebook;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.domain.Note;
import com.example.domain.NoteComparatorDef;

import java.util.List;

abstract class NotesViewModel extends ViewModel {

    abstract void remove(@NonNull Note note);
    abstract void applyBundleIfNeeded(@NonNull Bundle bundle);
    @NonNull
    abstract LiveData<List<Note>> getFilteredNotes();
    @NonNull
    abstract LiveData<String> getComparatorName();
    @NonNull
    abstract LiveData<Boolean> getFetchingData();
    @NonNull
    abstract LiveData<String> getFilter();
    abstract void setFilter(@Nullable String filter);
    abstract void setComparatorName(@NonNull @NoteComparatorDef String comparatorName);
    @NonNull
    abstract Note getNote(int id);
    abstract void addNote(@NonNull Note note);
    abstract void editNote(@NonNull Note note);
}
