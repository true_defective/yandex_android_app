package com.example.data;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.example.domain.Note;
import com.example.domain.NoteInteractor;
import com.example.domain.NotesDbRepository;
import com.example.domain.NotesServiceRepository;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class NoteInteractorImpl implements NoteInteractor {

    @NonNull
    private final CompositeDisposable mDisposable = new CompositeDisposable();
    @NonNull
    private final NotesDbRepository mNotesDbRepository;
    @NonNull
    private final NotesServiceRepository mNotesServiceRepository;

    public NoteInteractorImpl(@NonNull NotesServiceRepository notesServiceRepository,
                       @NonNull NotesDbRepository notesDbRepository) {
        mNotesDbRepository = notesDbRepository;
        mNotesServiceRepository = notesServiceRepository;
    }

    @Override
    public void fetchNotes(@NonNull MutableLiveData<Boolean> isFetching) {
        mNotesServiceRepository.getNotes().observeOn(AndroidSchedulers.mainThread())
                               .subscribe(new SingleObserver<List<Note>>() {
                                   @Override
                                   public void onSubscribe(Disposable d) {
                                       mDisposable.add(d);
                                   }

                                   @Override
                                   public void onSuccess(List<Note> notes) {
                                       mNotesDbRepository.fetchNotes(isFetching, notes);
                                   }

                                   @Override
                                   public void onError(Throwable e) {
                                       isFetching.setValue(false);
                                   }
                               });
    }

    @Override
    public void dispose() {
        mDisposable.dispose();
    }
}
