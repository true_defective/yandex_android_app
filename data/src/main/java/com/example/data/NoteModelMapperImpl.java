package com.example.data;

import android.support.annotation.NonNull;

import com.example.domain.Note;

import java.util.ArrayList;
import java.util.List;

public class NoteModelMapperImpl {
    @NonNull
    private Note fromEntity(@NonNull NoteEntity entity) {
        return new Note(entity.id, entity.header, entity.body, entity.date, entity.color, entity.outsideId);
    }

    @NonNull
    List<Note> fromEntities(@NonNull List<NoteEntity> entities) {
        List<Note> notes = new ArrayList<>();
        for (NoteEntity entity : entities) {
            notes.add(fromEntity(entity));
        }

        return notes;
    }

    @NonNull
    NoteEntity toEntity(@NonNull Note model) {
        return new NoteEntity(model.id, model.header, model.body, model.date, model.color, model.outsideId);
    }

    @NonNull
    List<NoteEntity> toEntities(@NonNull List<Note> models) {
        List<NoteEntity> entities = new ArrayList<>();
        for (Note note : models) {
            entities.add(toEntity(note));
        }

        return entities;
    }
}
