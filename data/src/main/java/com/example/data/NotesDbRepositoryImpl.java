package com.example.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.domain.Note;
import com.example.domain.NotesDbRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import io.reactivex.Flowable;

public class NotesDbRepositoryImpl implements NotesDbRepository {

    @NonNull
    private final Executor mExecutor = Executors.newSingleThreadExecutor();
    @NonNull
    private final NoteDao mNoteDao;
    @NonNull
    private final Flowable<List<Note>> mAllNotes;
    @NonNull
    private final NoteModelMapperImpl mNoteModelMapper;

    public NotesDbRepositoryImpl(@NonNull NoteDao noteDao, @NonNull NoteModelMapperImpl noteModelMapper) {
        mNoteDao = noteDao;
        mNoteModelMapper = noteModelMapper;
        mAllNotes = mNoteDao.getLiveDataAllNotes().map(mNoteModelMapper::fromEntities);
    }

    @NonNull
    @Override
    public Flowable<List<Note>> getAllNotes() {
        return mAllNotes;
    }

    @Override
    public void fetchNotes(@NonNull MutableLiveData<Boolean> isFetchingData, @Nullable List<Note> notes) {
        mExecutor.execute(() -> {
            if (notes == null) {
                isFetchingData.postValue(false);
                return;
            }
            List<NoteEntity> dbNoteEntities = mNoteDao.getListAllNotes();
            if (dbNoteEntities.isEmpty()) {
                insertAll(isFetchingData, notes);
            } else {
                List<Note> dbNotes = mNoteModelMapper.fromEntities(dbNoteEntities);
                for (Note note : notes) {
                    boolean wasInDb = false;
                    for (Note dbNote : dbNotes) {
                        if (dbNote.outsideId == 0 && dbNote.hasEqualContent(note)) {
                            update(new Note(dbNote.id, note.header, note.body, note.date, note.color, note.id));
                            wasInDb = true;
                            break;
                        }
                        if (dbNote.outsideId == note.id) {
                            update(new Note(dbNote.id, note.header, note.body, note.date, note.color, dbNote.outsideId));
                            wasInDb = true;
                            break;
                        }

                    }
                    if (!wasInDb) {
                        insert(note);
                    }
                }
                isFetchingData.postValue(false);
            }
        });
    }

    private void insertAll(@NonNull MutableLiveData<Boolean> isFetchingData, @NonNull List<Note> notes) {
        mExecutor.execute(() -> {
            for (Note note : notes) {
                note.setAutoId();
            }
            mNoteDao.insertAll(mNoteModelMapper.toEntities(notes));
            isFetchingData.postValue(false);
        });
    }

    @Override
    public void update(@NonNull Note note) {
        mExecutor.execute(() -> mNoteDao.update(mNoteModelMapper.toEntity(note)));
    }

    @Override
    public void insert(@NonNull Note note) {
        mExecutor.execute(() -> {
            note.setAutoId();
            mNoteDao.insert(mNoteModelMapper.toEntity(note));
        });
    }

    @Override
    public void delete(@NonNull Note note) {
        mExecutor.execute(() -> {
            mNoteDao.delete(mNoteModelMapper.toEntity(note));
        });
    }
}
