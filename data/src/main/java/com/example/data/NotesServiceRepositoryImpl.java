package com.example.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.domain.Note;
import com.example.domain.NotesServiceRepository;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotesServiceRepositoryImpl implements NotesServiceRepository {
    @NonNull
    private final Executor mExecutor = Executors.newSingleThreadExecutor();
    @NonNull
    private final NoteServiceApi mService;

    public NotesServiceRepositoryImpl(@NonNull NoteServiceApi service) {
        mService = service;
    }

    @NonNull
    @Override
    public Single<List<Note>> getNotes() {
        return Single.fromCallable(() -> mService.listNotes().execute().body())
                .subscribeOn(Schedulers.io());
    }

    @NonNull
    @Override
    public Single<Note> insert(@NonNull Note note) {
        return Single.fromCallable(() -> mService.createNote(note).execute().body())
                     .subscribeOn(Schedulers.io());
    }

    @Override
    public void update(@NonNull Note note) {
        mExecutor.execute(() -> {
            try {
                if (note.outsideId == 0) {
                    insert(note).subscribe();
                } else {
                    mService.updateNote(note.outsideId, note).execute();
                }
            } catch (IOException e) {
                e.printStackTrace();
                waitRequestTimeout();
                update(note);
            }
        });
    }

    @Override
    public void delete(@NonNull Note note) {
        mExecutor.execute(() -> {
            try {
                if (note.outsideId != 0) {
                    mService.deleteNote(note.outsideId).execute();
                }
            } catch (IOException e) {
                e.printStackTrace();
                waitRequestTimeout();
                delete(note);
            }
        });
    }

    private void waitRequestTimeout() {
        try {
            Thread.sleep(NoteServiceApi.REQUEST_TIMEOUT_MS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
