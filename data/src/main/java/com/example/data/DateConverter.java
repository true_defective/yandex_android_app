package com.example.data;

import android.arch.persistence.room.TypeConverter;
import android.support.annotation.NonNull;

import java.util.Date;

class DateConverter {
    @TypeConverter
    @NonNull
    public static Date fromTimestamp(long value) {
        return new Date(value);
    }

    @TypeConverter
    public static long dateToTimestamp(@NonNull Date date) {
        return date.getTime();
    }
}
