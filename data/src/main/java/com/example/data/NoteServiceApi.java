package com.example.data;

import android.support.annotation.NonNull;

import com.example.domain.Note;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface NoteServiceApi {
    int REQUEST_TIMEOUT_MS = 15000;

    @GET("api/notes/")
    Call<List<Note>> listNotes();

    @POST("api/notes/")
    Call<Note> createNote(@Body @NonNull Note note);

    @PUT("api/notes/{id}/")
    Call<Note> updateNote(@Path("id") int id, @Body @NonNull Note note);

    @DELETE("api/notes/{id}/")
    Call<Note> deleteNote(@Path("id") int id);
}
