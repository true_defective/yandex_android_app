package com.example.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

@Entity(tableName = "note_table")
public class NoteEntity {
    private static final int AUTO_ID = 0;

    @PrimaryKey(autoGenerate = true)
    public int id;
    @NonNull
    @SerializedName("title")
    public String header;
    @NonNull
    @SerializedName("description")
    public String body;
    @NonNull
    @SerializedName("creation_data")
    public Date date;
    @NonNull
    @SerializedName("color")
    public int color;

    @Expose
    public int outsideId;

    public NoteEntity(int id, @NonNull String header, @NonNull String body, @NonNull Date date, int color, int outsideId) {
        this.id = id;
        this.header = header;
        this.body = body;
        this.date = date;
        this.color = color;
        this.outsideId = outsideId;
    }
}
