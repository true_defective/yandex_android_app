package com.example.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.support.annotation.NonNull;

@Database(entities = {NoteEntity.class}, version = 1, exportSchema = false)
@TypeConverters({DateConverter.class})
public abstract class NoteRoomDatabase extends RoomDatabase {
    private static volatile NoteRoomDatabase INSTANCE;

    @NonNull
    public static NoteRoomDatabase getDatabase(@NonNull Context context) {
        if (INSTANCE == null) {
            synchronized (NoteRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                                                    NoteRoomDatabase.class,
                                                    "note_db").build();
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    public abstract NoteDao noteDao();
}
