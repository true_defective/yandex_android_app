package com.example.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface NoteDao {
    @Insert
    void insertAll(@NonNull List<NoteEntity> notes);

    @Insert
    void insert(@NonNull NoteEntity note);

    @Update
    void update(@NonNull NoteEntity note);

    @Delete
    void delete(@NonNull NoteEntity note);

    @Query("SELECT * FROM note_table WHERE id = :id LIMIT 1")
    @Nullable
    NoteEntity getNote(int id);

    @Query("SELECT * FROM note_table")
    @NonNull
    Flowable<List<NoteEntity>> getLiveDataAllNotes();

    @Query("SELECT * FROM note_table")
    @NonNull
    List<NoteEntity> getListAllNotes();
}
